import processing.sound.*;
SoundFile file;

void setup() {
  fullScreen();
  file = new SoundFile(this, "ons.mp3");
  file.play(); 
  //file.jump(27);
}

int c;
boolean satanIsHere = false;

void draw() {
  if (!satanIsHere) {
    colorMode(HSB);
    if (c >= 255)  c=0;  else  c++;
    background(c,255,255);
    colorMode(RGB);
    
    
    PImage logo=loadImage("Chien et chat.jpg");
    PImage patate=loadImage("Patate.png");
    PImage licorne=loadImage("Licorne.png");
    image(logo,550,20,240,240);
    image(patate,20,300,200,200);
    image(licorne,1160,300,200,200);
    
    fill(255);
    PFont love;
    love = createFont("Happy Loves.ttf",32);
    textFont(love);
    textSize(50);
    String s="Ceci est une family version d'un programme que j'ai créé";
    String t="Ceci comprend que des choses mignonnes et familiales";
    String u="Il se peut que le programme redevienne NOT FAMILY";
    text(s,20,20, 500, 500);
    text(t,850,20,500,500);
    text(u,225,400,1350,200);
    
    translate(500,550);
    smooth();
    noStroke();
    scale(4.0);
    fill(255, 0, 0);
    beginShape();
    vertex(50, 15);
    bezierVertex(50, -5, 75, 5, 50, 45);
    vertex(50, 15);
    bezierVertex(50, -5, 25, 5, 50, 45);
    endShape();
  } else {
    clear();
    background(8);
    PFont runes;
    runes = createFont("firstordersemital.ttf",32);
    textFont(runes);
    textSize(30);
    fill(20);
    String d="Sossure, Stonks, Patate, I am a cup, J'ai un BACKGROUND, le saint brocoli";
    fill(119,0,0);
    String a="Qui dominatur in nomine Domini in terris et ignem, et aerem, surge e medio et dominus noster ut eriperet nos a potestate diaboli dæmoniorum! Memento nobis, in terris quis fratribus vestris, sicut vobis operatur ad gloriam Lucifer! Dominus de qua remotos tristis et nigra flamma dat potestatem et invoco in unum! DIABOLUS tribuat. Vobis omnipotens aeterne, offero tibi, Corpus et anima mea! Maledictus vir alius similis tui deos ipsos dampnare me et te usque in sempiternum?";
    String b="Veni Domine, quia satanas es turrim ex potentia et imperium tuum et filios praedicate tibi Dominus omnium temporum! Fervore furentis et dispersos multitudine caeci videant oculi tui superna quaerunt quis arcu color cerae digitis alienis et voluptatibus carnis renuntians furiosi subrepens monumentis. Veteris vaporibus paradisus est fata! Bona opinio hominum fata stulti! Adiuva nos in hac nocte, et posse de nobis putabis tenebris Dominus!";
    String c="Tu virga dynamic attracting multorum millium tumescent nobis voluptate! Gaudium vos estis carnales estis, et ad insaniam motus ab agente ferri in mentis excessu! Manent membra vestra templum est Pantheon carnes! Audi nos satanas ut lucifer Benedictionem Tuam: in pace et in carne et in delicias animi! TRUCULENTUS SUPPORT US Domini!";
    text(a,20,20, 500, 500);
    text(b,850,20,500,500);
    text(c,20,550,1350,200);
  
    PImage satan=loadImage("Satan.jpg");
    image(satan,550,20,240,240);
  
    translate(width*0.5, height*0.5);
    rotate(frameCount / -50.0);
    circle(0,0,200);
    stroke(0,0,7);
    star(0, 0, 45, 100, 5); 
    rotate(120);
    polygon(0,0,45,5);     
  }
  
  if (mousePressed == true) {
    satanIsHere = true;
  }
}

void star(float x, float y, float radius1, float radius2, int npoints) {
  float angle = TWO_PI / npoints;
  float halfAngle = angle/2.0;
  beginShape();
  for (float a = 0; a < TWO_PI; a += angle) {
    float sx = x + cos(a) * radius2;
    float sy = y + sin(a) * radius2;
    vertex(sx, sy);
    sx = x + cos(a+halfAngle) * radius1;
    sy = y + sin(a+halfAngle) * radius1;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}

void polygon(float x, float y, float radius, int npoints) {
  float angle = TWO_PI / npoints;
  beginShape();
  for (float a = 0; a < TWO_PI; a += angle) {
    float sx = x + cos(a) * radius;
    float sy = y + sin(a) * radius;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}
